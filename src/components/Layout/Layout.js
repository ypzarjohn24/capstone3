import React from "react";
import Header from "../Header/Header.js";
import Footer from "../Footer/Footer.js";
import Routers from "../../routers/Routers.js";

// import AdminNav from "../../routers/Routers.js";
// import { useLocation } from "react-router-dom";

const Layout = () => {
  // const location = useLocation();

  return (
    <>
      {/* {location.pathname.startsWith("/dashboard") ? <AdminNav /> : } */}
      <Header />
      <div>
        <Routers />
      </div>
      <Footer />
    </>
  );
};

export default Layout;
