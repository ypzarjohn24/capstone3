import React from "react";
import ProductCard from "./ProductCard.js";

const ProductsList = ({ data }) => {
  if (!data || data.length === 0) {
    return <p>No products available.</p>;
  }
  return (
    <>
      {data?.map((item, index) => (
        <ProductCard key={index} item={item} />
      ))}
    </>
  );
};

export default ProductsList;
