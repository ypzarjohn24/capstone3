import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import Home from "../pages/Home";
import Products from "../pages/Products";
import Cart from "../pages/Cart";
import ProductDetails from "../pages/ProductDetails";
import Checkout from "../pages/Checkout";
import Login from "../pages/Login.js";
import Logout from "../pages/Logout.js";
import Register from "../pages/Register.js";
import Profile from "../pages/Profile.js";
// import Dashboard from "../admin/Dashboard.js";
// import AddProducts from "../admin/Dashboard.js";
// import AllProducts from "../admin/Dashboard.js";
import NotFound from "../pages/NotFound.js";

import { UserProvider } from "../UserContext.js";

const Routers = () => {
  return (
    <UserProvider>
      <Routes>
        <Route path="/" element={<Navigate to="home" />} />
        <Route path="/home" element={<Home />} />
        <Route path="/products" element={<Products />} />
        <Route path="/products/:id" element={<ProductDetails />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/checkout" element={<Checkout />} />
        <Route path="/login" element={<Login />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/profile" element={<Profile />} />
        <Route path="/register" element={<Register />} />

        {/* <Route path="/dashboard/add-product" element={<AddProducts />} /> */}

        {/* <Route path="/dashboard/all-products" element={<AllProducts />} />
        <Route path="/dashboard" element={<Dashboard />} /> */}

        <Route path="*" element={<NotFound />} />
      </Routes>
    </UserProvider>
  );
};

export default Routers;
