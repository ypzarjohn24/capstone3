import { createSlice } from "@reduxjs/toolkit";

// const initialState = {
//   isAuthenticated: false,
// };

const userSlice = createSlice({
  name: "user",
  initialState: {
    id: null,
    isAdmin: false,
  },
  reducers: {
    loginUser(state, action) {
      state.id = action.payload.id;
      state.isAdmin = action.payload.isAdmin;
    },
    logoutUser(state) {
      state.id = null;
      state.isAdmin = false;
    },
    setUserDetails: (state, action) => {
      state.id = action.payload.id;
      state.isAdmin = action.payload.isAdmin;
    },
  },
});

export const { loginUser, logoutUser, setUserDetails } = userSlice.actions;

export default userSlice.reducer;
