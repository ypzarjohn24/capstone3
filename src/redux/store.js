import { configureStore } from "@reduxjs/toolkit";
import cartSlice from "./slices/cartSlice.js";
import userReducer from "./slices/userSlice.js";

const store = configureStore({
  reducer: {
    user: userReducer,
    cart: cartSlice,
  },
});

export default store;
