const serviceData = [
  {
    icon: "ri-truck-line",
    title: "Free Shipping",
    subtitle:
      " Enjoy the convenience of free shipping on all orders, ensuring your shopping experience is both seamless and cost-effective.",
    bg: "#fdefe6",
  },
  {
    icon: "ri-refresh-line",
    title: "Easy Returns",
    subtitle:
      "We understand that sometimes choices change, which is why we offer a hassle-free returns process.",
    bg: "#ceebe9",
  },
  {
    icon: "ri-secure-payment-line",
    title: "Secure Payment",
    subtitle:
      "Shop with peace of mind, knowing that your personal and financial information is well-protected.",
    bg: "#e2f2b2",
  },
  {
    icon: "ri-exchange-dollar-line",
    title: " Back Guarantee",
    subtitle:
      "We believe in the quality of our offerings, which is why we back every purchase with our comprehensive satisfaction guarantee. ",
    bg: "#d6e5fb",
  },
];

export default serviceData;
