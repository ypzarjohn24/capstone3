import productImg01 from "../images/productImg01.png";
import productImg02 from "../images/productImg02.png";
import productImg03 from "../images/productImg03.png";
import productImg04 from "../images/productImg04.png";
import productImg05 from "../images/productImg05.png";
import productImg06 from "../images/productImg06.png";
import productImg07 from "../images/productImg07.png";
import productImg08 from "../images/productImg08.png";
import productImg09 from "../images/productImg09.png";
import productImg10 from "../images/productImg10.png";
import productImg11 from "../images/productImg11.png";
import productImg12 from "../images/productImg12.png";
import productImg13 from "../images/productImg13.png";
import productImg14 from "../images/productImg14.png";
import productImg15 from "../images/productImg15.png";
import productImg16 from "../images/productImg16.png";
import productImg17 from "../images/productImg17.png";
import productImg18 from "../images/productImg18.png";
import productImg19 from "../images/productImg19.png";
import productImg20 from "../images/productImg20.png";
import productImg21 from "../images/productImg21.png";
import productImg22 from "../images/productImg22.png";
import productImg23 from "../images/productImg23.png";
import productImg24 from "../images/productImg24.png";

const products = [
  {
    id: "01",
    productName: "Nike Fly.By Mid 3",
    imgUrl: productImg01,
    category: "trending",
    price: 2399,
    shortDesc: "PLAY FAST AND FRESH ALL GAME LONG..",
    description:
      "Cushioning, ankle support and traction are essential on the basketball court. The Nike Fly.By Mid 3 delivers exactly what's needed to help players excel on offence and defence. Designed to look as comfortable as it feels, the mid-top sneaker has plush, energy-returning foam to help keep you fresh through four quarters.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },

  {
    id: "02",
    productName: "Nike GT Cut 2 EP",
    imgUrl: productImg02,
    category: "trending",
    price: 9095,
    shortDesc: "Enhance Your 1st Step",
    description:
      "The GT Cut 2 EP helps you stop in an instant and accelerate back into the open lane in a low-to-the-ground design that helps minimise court contact while switching direction.",

    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },

  {
    id: "03",
    productName: "Nike Impact 4",
    imgUrl: productImg03,
    category: "trending",
    price: 4795,
    shortDesc: "Elevate your game and your hops.",
    description:
      " Charged with Max Air cushioning in the heel, this lightweight, secure shoe helps you get off the ground confidently and land comfortably.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },

  {
    id: "04",
    productName: "KD15 EP",
    imgUrl: productImg04,
    category: "trending",
    price: 6719,
    shortDesc: "Broken-In Comfort",
    description:
      " Game-making plays and impossible shots are all in a day's work for Kevin Durant.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "05",
    productName: "Giannis Immortality 3 EP",
    imgUrl: productImg05,
    category: "best-sales",
    price: 4295,
    shortDesc: "Mindfully made for today's high-paced, position-less game",
    description:
      " How do you want your game to be remembered? Preserve your place among the greats, like Giannis, in the Giannis Immortality 3.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "06",
    productName: "Sabrina 1 'Spark' EP",
    imgUrl: productImg06,
    category: "best-sales",
    price: 6895,
    shortDesc: "Play with Confidence",
    description:
      "Sabrina Ionescu's game is unique, hard to define and built with an undeniable craft.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "07",
    productName: "Nike Precision 6",
    imgUrl: productImg07,
    category: "best-sales",
    price: 3495,
    shortDesc: "THE QUINTESSENCE OF QUICK.",
    description:
      " Create space, stop in an instant, shoot off the dribble—do it all with the Nike Precision 6.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "08",
    productName: "Jordan Why Not .6 PF",
    imgUrl: productImg08,
    category: "best-sales",
    price: 6719,
    shortDesc: "Lightweight Containment",
    description:
      "Russell Westbrook's 6th signature shoe is—you guessed it—all about speed.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "09",
    productName: "Nike Dunk Low Retro",
    imgUrl: productImg09,
    category: "lifestyle",
    price: 5495,
    shortDesc:
      "Channel vintage style back onto the streets with aged and distressed details.",
    description:
      "The '80s b-ball icon returns with mixed materials and throwback hoops flair. ",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "10",
    productName: "Air Jordan 1 Low FlyEase",
    imgUrl: productImg10,
    category: "lifestyle",
    price: 7395,
    shortDesc: "Leather upper provides a structured feel and a premium look.",
    description:
      "Lock in your style with this AJ1. We kept everything you love about the classic design—premium leather, Air cushioning, iconic Wings logo—while adding the Nike FlyEase closure system to make on and off a breeze.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "11",
    productName: "Nike Killshot 2 Leather",
    imgUrl: productImg11,
    category: "lifestyle",
    price: 5195,
    shortDesc: "The rubber gum sole adds a retro look and durable traction.",
    description:
      "Inspired by the original low-profile tennis shoe, the Nike Killshot 2 updates the upper with a variety of textured leathers to create a fresh look.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "12",
    productName: "Jordan Series ES",
    imgUrl: productImg12,
    category: "lifestyle",
    price: 4495,
    shortDesc:
      "Suede and smooth leather are combined with a sleek, low-cut silhouette for versatile styling..",
    description:
      "Inspired by Mike's backyard battles with his older brother Larry, the Jordan Series references their legendary sibling rivalry throughout the design. ",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "13",
    productName: "KD Trey 5 X EP",
    imgUrl: productImg13,
    category: "basketball",
    price: 4795,
    shortDesc:
      "This version is made for outdoor-court use with its extra-durable rubber outsole.",
    description:
      "With its lightweight upper and plush support system, the KD Trey 5 X can help you float like KD, waiting for the perfect moment to drive to the hoop. A secure midfoot strap is suited for scoring binges and defensive stands, so that you can lock in and keep winning.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },

  {
    id: "14",
    productName: "LeBron Witness 7 EP",
    imgUrl: productImg14,
    category: "basketball",
    price: 5295,
    shortDesc:
      "This version is made for outdoor court use with its extra-durable rubber outsole.",
    description:
      "The longer LeBron's legendary career continues, the more his game needs a design that doesn't weigh him down yet can still control all that sublime power.",

    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },

  {
    id: "15",
    productName: "Nike GT Jump",
    imgUrl: productImg15,
    category: "basketball",
    price: 7679,
    shortDesc:
      "The double-stacked Zoom Air works in unison with an external jump frame made from PEBAX® that's lightweight, springy and strong.",
    description:
      "The woven exoskeleton-like upper helps you feel fully connected, so you can move with confidence and power and defeat the force that holds you down.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },

  {
    id: "16",
    productName: "Nike Cosmic Unity 2",
    imgUrl: productImg16,
    category: "basketball",
    price: 6479,
    shortDesc:
      "Celebrate the love and joy of the game with the Nike Cosmic Unity 2. ",
    description:
      "This shoe will help keep you fresh and secure without overloading it with extra grams, so that you can focus on locking down the perimeter defensively or starting the fast break after a rebound.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "17",
    productName: "Nike InfinityRN 4",
    imgUrl: productImg17,
    category: "running",
    price: 8895,
    shortDesc:
      "The more supportive the shoe, the more stability it can give to your natural stride.",
    description:
      "This version of the Nike InfinityRN 4 still provides a plush and smooth ride, with the new ReactX foam delivering more support. The revamped Flyknit upper and wider toe box help your foot feel stable and secure.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "18",
    productName: "Nike Pegasus 40 Premium",
    imgUrl: productImg18,
    category: "running",
    price: 6319,
    shortDesc:
      "A springy ride for any run, the Peg's familiar, just-for-you feel returns to help you accomplish your goals.",
    description:
      "This version has the same responsiveness and neutral support you love but with improved comfort in those sensitive areas of your foot, like the arch and toes. ",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "19",
    productName: "Nike Streakfly",
    imgUrl: productImg19,
    category: "running",
    price: 5159,
    shortDesc:
      "A full-length ZoomX midsole provides optimal lightweight responsiveness.",
    description:
      "Our lightest racing shoe, the Nike Streakfly is all about the speed you need to take on the competition in a mile, 5K or 10K race. Low profile with sleek details, it feels like it disappears on your foot to help you better connect with the road on the way to your personal best.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "20",
    productName: "Nike Downshifter 12",
    imgUrl: productImg20,
    category: "running",
    price: 2995,
    shortDesc:
      "Mesh throughout the upper has a lightweight, breathable feel. The mesh was placed in key zones based on runner feedback, providing you with cooling where it counts.",
    description:
      "Take those first steps on your running journey in the Nike Downshifter 12. Made from at least 20% recycled content by weight, it has a supportive fit and stable ride, with a lightweight feel that easily transitions from your workout to hangout. Your trek begins. Lace up and hit the road.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "21",
    productName: "Jordan Stay Loyal 2",
    imgUrl: productImg21,
    category: "jordan",
    price: 6395,
    shortDesc:
      "Inspired by generations of Js, these kicks are a collage of cool.",
    description:
      "Design details recall decades of legendary shoes while paying homage to MJs storied career. Breathable mesh, sturdy leather and lightweight Air cushioning in the heel make it that much easier to walk in the footsteps of greatness.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "22",
    productName: "Air Jordan 7 Retro",
    imgUrl: productImg22,
    category: "jordan",
    price: 10895,
    shortDesc: "Real and synthetic leather and textile materials in the upper.",
    description:
      "Inspired by the shoe originally worn by MJ during the '92 season and summer of basketball, the Air Jordan 7 Retro revives its championship legacy for a new generation of sneakerheads.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "23",
    productName: "Air Jordan 1 Retro High OG Craft",
    imgUrl: productImg23,
    category: "jordan",
    price: 7919,
    shortDesc: "Turn heads from dawn 'til dusk in the Air Jordan 1.",
    description:
      "Premium denim on the upper is dyed with a light-to-dark gradient, shifting seamlessly from alpenglow to twilight. The asymmetrical design lets you create an atmosphere that's all your own—and the finishing touch? Nike Air cushioning underfoot lets you take your game anywhere in comfort. Sky's the limit with this one.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
  {
    id: "24",
    productName: "Jordan Max Aura 5",
    imgUrl: productImg24,
    category: "jordan",
    price: 7395,
    shortDesc:
      "When you need a shoe that's ready 24/7, it's gotta be the Max Aura 5. ",
    description:
      "Inspired by the AJ3, this pair of kicks puts a modern spin on the classic. They're made from durable leather and textiles that sit atop a heel of Nike Air cushioning so you can walk, run or skate all day and still have fresh-feeling soles.",
    reviews: [
      {
        rating: 4.7,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
      },
    ],
    avgRating: 4.5,
  },
];

export default products;
