import React, { useState } from "react";
import CommonSection from "../components/UI/CommonSection";
import Helmet from "../components/Helmet/Helmet";
import { Container, Row, Col } from "react-bootstrap";
import "../styles/products.css";
import products from "../assets/data/products";
import ProductsList from "../components/UI/ProductsList";

const Products = () => {
  const [productsData, setProductsData] = useState(products);

  const handleFilter = (event) => {
    const filterValue = event.target.value;
    if (filterValue === "lifestyle") {
      const filteredProducts = products.filter(
        (item) => item.category === "lifestyle"
      );

      setProductsData(filteredProducts);
    }

    if (filterValue === "running") {
      const filteredProducts = products.filter(
        (item) => item.category === "running"
      );

      setProductsData(filteredProducts);
    }

    if (filterValue === "basketball") {
      const filteredProducts = products.filter(
        (item) => item.category === "basketball"
      );

      setProductsData(filteredProducts);
    }

    if (filterValue === "jordan") {
      const filteredProducts = products.filter(
        (item) => item.category === "jordan"
      );

      setProductsData(filteredProducts);
    }
  };

  const handleSearch = (event) => {
    const searchTerm = event.target.value;

    const searchedProducts = products.filter((item) =>
      item.productName.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setProductsData(searchedProducts);
  };
  return (
    <Helmet title="products">
      <CommonSection title="Products" />
      <section>
        <Container>
          <Row>
            <Col lg="3" md="6">
              <div className="filter__widget">
                <select onClick={handleFilter}>
                  <option>Filter By Category</option>
                  <option value="lifestyle">Lifestyle</option>
                  <option value="running">Running</option>
                  <option value="basketball">Basketball</option>
                  <option value="jordan">Jordan</option>
                </select>
              </div>
            </Col>
            <Col lg="3" md="6" className="text-end">
              <div className="filter__widget">
                <select>
                  <option>Sort By</option>
                  <option value="ascending">ascending</option>
                  <option value="descending">descending</option>
                </select>
              </div>
            </Col>
            <Col lg="6" md="12">
              <div className="search__box">
                <input
                  type="text"
                  placeholder="Search....."
                  onChange={handleSearch}
                />
                <span>
                  <i classNames="ri-search-line"></i>
                </span>
              </div>
            </Col>
          </Row>
        </Container>
      </section>

      <section className="pt-0">
        <Container>
          <Row>
            {productsData.length === 0 ? (
              <h1 className="text-center fs-4">No products are found!</h1>
            ) : (
              <ProductsList data={productsData} />
            )}
          </Row>
        </Container>
      </section>
    </Helmet>
  );
};

export default Products;
