import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import Helmet from "../components/Helmet/Helmet.js";
import { Container, Row, Col } from "react-bootstrap";
import heroImg from "../assets/images/hero-image.png";
import "../styles/home.css";
import Services from "../services/Services.js";
import ProductsList from "../components/UI/ProductsList.js";
import bestClassicShoes from "../assets/images/best-classic-shoes.png";
import products from "../assets/data/products.js";

const Home = () => {
  const [trendingProducts, setTrendingProducts] = useState([]);
  const [bestSalesProducts, setBestSalesProducts] = useState([]);

  useEffect(() => {
    const filteredTrendingProducts = products.filter(
      (item) => item.category === "trending"
    );

    const filteredBestSalesProducts = products.filter(
      (item) => item.category === "best-sales"
    );

    setTrendingProducts(filteredTrendingProducts);
    setBestSalesProducts(filteredBestSalesProducts);
  }, []);
  return (
    <Helmet title={"Home"}>
      <section className="hero__section">
        <Container>
          <Row>
            <Col lg="6" md="6">
              <div className="hero__content">
                <h2 className="hero__subtitle">
                  Welcome to our premium online destination for basketball men's
                  shoes!
                </h2>
                <p>
                  Step up your game with our exclusive collection of
                  high-performance basketball footwear designed to elevate your
                  performance on the court.
                </p>
                <motion.button whileTap={{ scale: 1.2 }} className="buy__btn">
                  <Link to="/products">BUY NOW</Link>
                </motion.button>
              </div>
            </Col>
            <Col lg="6" md="6">
              <img src={heroImg} alt="hero-image" />
            </Col>
          </Row>
        </Container>
      </section>

      <Services />
      <section className="trending__products">
        <Container>
          <Row>
            <Col lg="12" className="text-center">
              <h2 className="section__title">Trending Products</h2>
            </Col>
            <ProductsList data={trendingProducts} />
          </Row>
        </Container>
      </section>

      <section className="best__sales">
        <Container>
          <Row>
            <Col lg="12" className="text-center">
              <h2 className="section__title">Best Sales</h2>
            </Col>
            <ProductsList data={bestSalesProducts} />
          </Row>
        </Container>
      </section>

      <section className="best__classic">
        <Container>
          <Row>
            <Col lg="6" md="6" className="d-flex align-items-center">
              <div className="classic__content">
                <h2 className="title">2023 Best Classical Shoes</h2>
                <p>
                  Nearly 2 decades into a career exceeding every lofty
                  expectation, LeBron James has refused to settle for anything
                  less than greatness, even when he was the one setting the
                  standard for generations to come. Now, his latest signature
                  shoe is lighter, low to the ground and turbo-like. The LeBron
                  XX (or LeBron 20) is unlike any design LeBron has donned
                  before—comfortable and supportive yet low-cut, feathery-fast
                  and created to stay ahead of today's frenzied style of play.
                  With its extra-durable rubber outsole, this version gives you
                  traction for outdoor courts.
                </p>
              </div>
            </Col>
            <Col lg="6" md="6">
              <div className="image__content d-flex align-items-center">
                <img src={bestClassicShoes} alt="classic-shoes" />
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </Helmet>
  );
};
export default Home;
