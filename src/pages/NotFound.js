export default function NotFound() {
  return (
    <div className="text-center my-5">
      <h1>Not Found</h1>
      <p>The page you are looking for could not be found!</p>
    </div>
  );
}
