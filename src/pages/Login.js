import React, { useState, useEffect } from "react";
import Helmet from "../components/Helmet/Helmet.js";
import { Container, Row, Col, Form, FormGroup } from "react-bootstrap";
import { Link, Navigate } from "react-router-dom";
import { toast } from "react-toastify";
import { useUserContext } from "../UserContext.js";

import { useDispatch } from "react-redux";
import { setUserDetails } from "../redux/slices/userSlice.js";

export default function Login() {
  const { user, setUser } = useUserContext();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  const dispatch = useDispatch();

  function authenticate(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result);

        if (result.accessToken) {
          localStorage.setItem("token", result.accessToken);
          localStorage.setItem("userId", result.userId);
          // window.location.reload();

          dispatch(
            setUserDetails({
              id: result.userId,
              isAdmin: result.isAdmin,
            })
          );

          retrieveUserDetails(result.accessToken, result.userId);

          setEmail("");
          setPassword("");

          toast.success("You have logged in successfully!");
        } else {
          toast.error(`${email} does not exist`);
        }
      });
  }

  const retrieveUserDetails = (token, userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: userId,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result && result._id) {
          setUser({
            id: result._id,
            isAdmin: result.isAdmin,
          });
        }
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <Helmet title="Login">
      <section>
        <Container>
          <Row>
            <Col lg="6" className="m-auto text-center">
              {user && user.id !== null ? (
                <Navigate to="/products" />
              ) : (
                <Form
                  className="auth__form"
                  onSubmit={(event) => authenticate(event)}
                >
                  <h3 className="fw-bold mb-4 text-light">Login</h3>
                  <FormGroup className="form__group">
                    <input
                      type="email"
                      placeholder="Enter your email"
                      value={email}
                      onChange={(event) => setEmail(event.target.value)}
                    />
                  </FormGroup>
                  <FormGroup className="form__group">
                    <input
                      type="password"
                      placeholder="Enter your password"
                      value={password}
                      onChange={(event) => setPassword(event.target.value)}
                    />
                  </FormGroup>

                  <button
                    type="submit"
                    className="buy__btn auth__btn"
                    disabled={!isActive}
                  >
                    Login
                  </button>

                  <p>
                    Don't have an account?
                    <Link to="/register"> Create an account</Link>
                  </p>
                </Form>
              )}
            </Col>
          </Row>
        </Container>
      </section>
    </Helmet>
  );
}
