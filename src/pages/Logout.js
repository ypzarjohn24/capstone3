import { Navigate } from "react-router-dom";
import { useEffect } from "react";
import { useUserContext } from "../UserContext.js";

export default function Logout() {
  const { setUser, unsetUser } = useUserContext();

  useEffect(() => {
    setUser({
      id: null,
      isAdmin: null,
    });
    unsetUser();
  }, [setUser, unsetUser]);

  return <Navigate to="/login" />;
}
