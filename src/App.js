import "./App.css";
import Layout from "./components/Layout/Layout.js";
import Routers from "./routers/Routers.js";
import { useEffect } from "react";
import { useUserContext } from "./UserContext.js";

function App() {
  const { setUser } = useUserContext();

  useEffect(() => {
    const fetchUserDetails = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/api/users/details`,
          {
            method: "POST",
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              id: localStorage.getItem("userId"),
            }),
          }
        );

        const result = await response.json();

        // .then((response) => response.json())
        // .then((result) => {
        if (typeof result._id !== "undefined") {
          setUser({
            id: result._id,
            isAdmin: result.isAdmin,
          });
        } else {
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      } catch (error) {
        console.error("Error fetching user details:", error);
      }
    };

    fetchUserDetails();
  }, []);

  return (
    <>
      <Layout>
        <Routers />
      </Layout>
    </>
  );
}

export default App;
